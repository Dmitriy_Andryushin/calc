package Calc;

/**
 * В данном классе происходят все вычисления
 * Данный класс умеет считать : сумму , разность
 * произведение , частное , остаток от деления , степень.
 *
 * @author Andryushin Dmitriy , 16it18k
 */

class CalculatorSystem{

    /**
     * Данный метод считает сумму 2-х чисел
     * @param a - первое слагаемое
     * @param b - второе слагаемое
     * @return a + b
     */

    static double amount(double a , double b){
        return a + b;
    }

    /**
     * Данный метод считает разность 2-х чисел
     * @param a - уменьшаемое число
     * @param b - вычитаемое число
     * @return a - b
     */

    static double difference(double a , double b){
        return a - b;
    }

    /**
     * Данный метод считает произведение 2-х чисел
     * @param a - первый множитель
     * @param b - второй множитель
     * @return a * b
     */

    static double multiplication(double a, double b){
        return a * b;
    }

    /**
     * Данный метод считает частное 2-х чисел
     * @param a - делимое число
     * @param b - делитель
     * @return a / b
     */
    static double division(double a , double b){
        return a / b;
    }

    /**
     * Данный метод считает остаток от деления
     * @param a - делимое число
     * @param b - делитель
     * @return a % b
     */

    static double rest(double a , double b){
        return a % b;
    }

    /**
     * Данный метод считает степень числа
     * @param a - считаемое число
     * @param b - степень возведения этого числа
     * @return a ^ b
     */

    static double degree(double a , int b){
        double result = 1 ;
        switch (b) {
            case 0 :
                result = 1;
                break;
            case 1 :
                result = a;
            default: for (int i = 0; i < b; i++) {
                result *= a;
            }
        }
        return result;
    }
}